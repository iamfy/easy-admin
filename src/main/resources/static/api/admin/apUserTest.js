/**
 * 分页查询测试用户列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apUserTest/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试用户详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apUserTest/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试用户
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apUserTest/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试用户
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apUserTest/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试用户
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apUserTest/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试用户
 *
 * @param query
 * @returns {*}
 */
function exportData(data) {
    return requests({
        url: '/admin/apUserTest/export',
        method: 'post',
        data: data
    })
}
