package com.mars.module.tool.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.tool.request.SysOperLogRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.tool.mapper.SysOperLogMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.tool.entity.SysOperLog;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.tool.service.ISysOperLogService;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Objects;

/**
 * 操作日志记录业务层处理
 *
 * @author mars
 * @date 2023-11-17
 */
@Slf4j
@Service
@AllArgsConstructor
public class SysOperLogServiceImpl implements ISysOperLogService {

    private final SysOperLogMapper baseMapper;

    @Override
    public SysOperLog add(SysOperLogRequest request) {
        SysOperLog entity = new SysOperLog();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    public boolean delete(Long operId) {
        return baseMapper.deleteById(operId) > 0;
    }

    @Override
    public boolean deleteBatch(List<Long> operIds) {
        return baseMapper.deleteBatchIds(operIds) > 0;
    }

    @Override
    public boolean update(SysOperLogRequest request) {
        SysOperLog entity = new SysOperLog();
        BeanUtils.copyProperties(request, entity);
        baseMapper.updateById(entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public SysOperLog getById(Long operId) {
        return baseMapper.selectById(operId);
    }

    @Override
    public PageInfo<SysOperLog> pageList(SysOperLogRequest request) {
        Page<SysOperLog> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<SysOperLog> query = this.buildWrapper(request);
        IPage<SysOperLog> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }

    private LambdaQueryWrapper<SysOperLog> buildWrapper(SysOperLogRequest param) {
        LambdaQueryWrapper<SysOperLog> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getTitle())) {
            query.like(SysOperLog::getTitle, param.getTitle());
        }
        if (StringUtils.isNotBlank(param.getMethod())) {
            query.like(SysOperLog::getMethod, param.getMethod());
        }
        if (StringUtils.isNotBlank(param.getRequestMethod())) {
            query.like(SysOperLog::getRequestMethod, param.getRequestMethod());
        }
        if (StringUtils.isNotBlank(param.getOperName())) {
            query.like(SysOperLog::getOperName, param.getOperName());
        }
        if (StringUtils.isNotBlank(param.getDeptName())) {
            query.like(SysOperLog::getDeptName, param.getDeptName());
        }
        if (StringUtils.isNotBlank(param.getOperUrl())) {
            query.like(SysOperLog::getOperUrl, param.getOperUrl());
        }
        if (StringUtils.isNotBlank(param.getOperIp())) {
            query.like(SysOperLog::getOperIp, param.getOperIp());
        }

        if (Objects.nonNull(param.getOperTime())) {
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            String time = sdf.format(param.getOperTime());
            query.apply("DATE_FORMAT(oper_time,'%Y-%m-%d')= '" + time + "'");
        }
        if (StringUtils.isNotBlank(param.getOperParam())) {
            query.like(SysOperLog::getOperParam, param.getOperParam());
        }
        if (StringUtils.isNotBlank(param.getJsonResult())) {
            query.like(SysOperLog::getJsonResult, param.getJsonResult());
        }
        if (param.getStatus() != null) {
            query.like(SysOperLog::getStatus, param.getStatus());
        }
        query.orderByDesc(SysOperLog::getOperTime);
        return query;
    }

}
