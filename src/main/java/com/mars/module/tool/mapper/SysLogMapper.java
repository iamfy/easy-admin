package com.mars.module.tool.mapper;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.common.request.log.LogApiQueryRequest;
import com.mars.common.response.log.LogApiResponse;
import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.tool.entity.SysLog;
import org.apache.ibatis.annotations.Param;

/**
 * 接口日志
 *
 * @author 源码字节-程序员Mars
 */
public interface SysLogMapper extends BasePlusMapper<SysLog> {

    /**
     * 分页查询
     *
     * @param page     page
     * @param queryDto queryDto
     * @return IPage<LogApiResponse>
     */
    IPage<LogApiResponse> selectPageList(@Param("page") IPage<Object> page, @Param("queryDto") LogApiQueryRequest queryDto);
}
