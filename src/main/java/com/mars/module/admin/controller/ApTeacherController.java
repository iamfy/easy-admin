package com.mars.module.admin.controller;


import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import com.mars.common.enums.BusinessType;
import com.mars.common.result.R;
import com.mars.common.util.ExcelUtils;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.RateLimiter;
import com.mars.module.admin.entity.ApTeacher;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import com.mars.common.response.PageInfo;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.IApTeacherService;
import com.mars.module.admin.request.ApTeacherRequest;

import javax.servlet.http.HttpServletResponse;

/**
 * 教师控制层
 *
 * @author mars
 * @date 2024-03-23
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "教师接口管理",tags = "教师接口管理")
@RequestMapping("/admin/apTeacher" )
public class ApTeacherController {

    private final IApTeacherService iApTeacherService;

    /**
     * 分页查询教师列表
     */
    @ApiOperation(value = "分页查询教师列表")
    @PostMapping("/pageList")
    public R<PageInfo<ApTeacher>> pageList(@RequestBody ApTeacherRequest apTeacherRequest) {
        return R.success(iApTeacherService.pageList(apTeacherRequest));
    }

    /**
     * 获取教师详细信息
     */
    @ApiOperation(value = "获取教师详细信息")
    @GetMapping(value = "/query/{id}")
    public R<ApTeacher> detail(@PathVariable("id") Long id) {
        return R.success(iApTeacherService.getById(id));
    }

    /**
     * 新增教师
     */
    @Log(title = "新增教师", businessType = BusinessType.INSERT)
    @RateLimiter
    @ApiOperation(value = "新增教师")
    @PostMapping("/add")
    public R<Void> add(@RequestBody ApTeacherRequest apTeacherRequest) {
        iApTeacherService.add(apTeacherRequest);
        return R.success();
    }

    /**
     * 修改教师
     */
    @Log(title = "修改教师", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改教师")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody ApTeacherRequest apTeacherRequest) {
        iApTeacherService.update(apTeacherRequest);
        return R.success();
    }

    /**
     * 删除教师
     */
    @Log(title = "删除教师", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除教师")
    @PostMapping("/delete/{ids}")
    public R<Void> remove(@PathVariable Long[] ids) {
        iApTeacherService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }

    /**
    * 导出教师
    *
    * @param response response
    * @throws IOException IOException
    */
    @PostMapping(value = "/export")
    public void exportExcel(HttpServletResponse response,@RequestBody ApTeacherRequest apTeacherRequest) throws IOException {
        List<ApTeacher> list = iApTeacherService.list(apTeacherRequest);
        ExcelUtils.exportExcel(list, ApTeacher.class, "教师信息", response);
    }
}
