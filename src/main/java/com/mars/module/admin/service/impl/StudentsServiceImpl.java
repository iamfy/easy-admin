package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.mars.common.base.UserContextInfo;
import com.mars.common.response.PageInfo;
import com.mars.framework.context.ContextUserInfoThreadHolder;
import com.mars.module.system.entity.BaseEntity;
import com.mars.module.system.service.ISysUserService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.StudentsRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.StudentsMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.Students;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IStudentsService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 学生成绩业务层处理
 *
 * @author mars
 * @date 2024-03-12
 */
@Slf4j
@Service
@AllArgsConstructor
public class StudentsServiceImpl implements IStudentsService {

    private final StudentsMapper baseMapper;

    private final ISysUserService sysUserService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Students add(StudentsRequest request) {
        Students entity = Students.builder().build();
        UserContextInfo info = ContextUserInfoThreadHolder.get();
        BeanUtils.copyProperties(request, entity);
        entity.setUserId(info.getId());
        entity.setName(info.getUserName());
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Integer id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Integer> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(StudentsRequest request) {
        Students entity = Students.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0;
    }

    @Override
    public Students getById(Integer id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<Students> pageList(StudentsRequest request) {
        Page<Students> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<Students> query = this.buildWrapper(request);
        IPage<Students> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<Students> list(StudentsRequest request) {
        LambdaQueryWrapper<Students> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    private LambdaQueryWrapper<Students> buildWrapper(StudentsRequest param) {
        LambdaQueryWrapper<Students> query = new LambdaQueryWrapper<>();
        if (StringUtils.isNotBlank(param.getName())) {
            query.like(Students::getName, param.getName());
        }
        if (param.getGender() != null) {
            query.eq(Students::getGender, param.getGender());
        }
        if (param.getEnglish() != null) {
            query.eq(Students::getEnglish, param.getEnglish());
        }
        if (param.getStatus() != null) {
            query.eq(Students::getStatus, param.getStatus());
        }
        UserContextInfo info = ContextUserInfoThreadHolder.get();
        boolean isAdmin = sysUserService.isAdmin(info.getId());
        if (!isAdmin) {
            query.eq(Students::getUserId, info.getId());
        }
        query.orderByDesc(BaseEntity::getCreateTime);
        return query;
    }

}
