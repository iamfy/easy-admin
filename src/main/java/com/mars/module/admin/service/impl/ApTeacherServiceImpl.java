package com.mars.module.admin.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mars.common.response.PageInfo;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.request.ApTeacherRequest;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.mars.module.admin.mapper.ApTeacherMapper;
import org.springframework.beans.BeanUtils;
import com.mars.module.admin.entity.ApTeacher;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mars.module.admin.service.IApTeacherService;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 教师业务层处理
 *
 * @author mars
 * @date 2024-03-23
 */
@Slf4j
@Service
@AllArgsConstructor
public class ApTeacherServiceImpl implements IApTeacherService {

    private final ApTeacherMapper baseMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public ApTeacher add(ApTeacherRequest request) {
        ApTeacher entity = ApTeacher.builder().build();
        BeanUtils.copyProperties(request, entity);
        baseMapper.insert(entity);
        return entity;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean delete(Long id) {
        return baseMapper.deleteById(id) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean deleteBatch(List<Long> ids) {
        return baseMapper.deleteBatchIds(ids) > 0;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public boolean update(ApTeacherRequest request) {
        ApTeacher entity = ApTeacher.builder().build();
        BeanUtils.copyProperties(request, entity);
        return baseMapper.updateById(entity) > 0 ;
    }

    @Override
    public ApTeacher getById(Long id) {
        return baseMapper.selectById(id);
    }

    @Override
    public PageInfo<ApTeacher> pageList(ApTeacherRequest request) {
        Page<ApTeacher> page = new Page<>(request.getPageNo(), request.getPageSize());
        LambdaQueryWrapper<ApTeacher> query = this.buildWrapper(request);
        IPage<ApTeacher> pageRecord = baseMapper.selectPage(page, query);
        return PageInfo.build(pageRecord);
    }


    @Override
    public List<ApTeacher> list(ApTeacherRequest request) {
        LambdaQueryWrapper<ApTeacher> query = this.buildWrapper(request);
        return baseMapper.selectList(query);
    }

    private LambdaQueryWrapper<ApTeacher> buildWrapper(ApTeacherRequest param) {
        LambdaQueryWrapper<ApTeacher> query = new LambdaQueryWrapper<>();
         if (StringUtils.isNotBlank(param.getName())){
               query.like(ApTeacher::getName ,param.getName());
        }
        return query;
    }

}
