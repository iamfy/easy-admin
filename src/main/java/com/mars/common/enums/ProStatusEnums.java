package com.mars.common.enums;

/**
 * 流程状态枚举
 *
 * @author 源码字节-程序员Mars
 */
public enum ProStatusEnums {

    /**
     * 未提交
     */
    NOT_SUBMITTED(1, "未提交"),

    /**
     * 流程中
     */
    PROCESSING(2, "流程中"),
    /**
     * 已完成
     */
    FINISHED(3, "已完成");


    private Integer status;

    private String message;

    ProStatusEnums() {
    }

    ProStatusEnums(Integer status, String message) {
        this.status = status;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
}
