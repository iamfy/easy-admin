package com.mars.framework.handler;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.mars.common.util.Md5Utils;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Random;

/**
 * 中文转英文Handler
 *
 * @author 程序员Mars
 * @version 1.0
 * @date 2023-11-14 17:02:29
 */
public class TransHandler {

    public static final String TRANS_API = "https://fanyi.youdao.com/translate_o?smartresult=dict&smartresult=rule";

    /**
     * 中文转英文
     *
     * @param word word
     * @return String
     */
    public static String trans(String word) {
        try {

            String ts = String.valueOf(System.currentTimeMillis());
            String salt = ts + new Random().nextInt(10);
            String signStr = "fanyideskweb" + word + salt + "Ygy_4c=r#e#4EX^NUGUc5";
            String sign = Md5Utils.getMd5(signStr);

            String data = "i=" + word + "&from=AUTO&to=AUTO&smartresult=dict&client=fanyideskweb&salt=" + salt + "&sign=" + sign +
                    "&lts=" + ts + "&bv=e70edeacd2efbca394a58b9e43a6ed2a&doctype=json&version=2.1&keyfrom=fanyi.web&action=FY_BY_CLICKBUTTION";
            URL url = new URL(TRANS_API);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
            connection.setRequestProperty("Cookie", "hb_MA-AF8F-B2A48FFDAF15_source=www.baidu.com; _ga=GA1.2.1518559015.1653052879; OUTFOX_SEARCH_USER_ID_NCOO=385550258.69743496; OUTFOX_SEARCH_USER_ID=\"-81754210@10.108.160.101\"; P_INFO=18483678377|1653052903|1|youdaonote|00&99|null&null&null#sic&511700#10#0|&0||18483678377; fanyi-ad-id=305838; fanyi-ad-closed=1; ___rl__test__cookies=1653660549450");
            connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/96.0.4664.93 Safari/537.36");
            connection.setRequestProperty("Referer", "https://fanyi.youdao.com/");
            connection.setDoOutput(true);

            OutputStream outputStream = connection.getOutputStream();
            outputStream.write(data.getBytes(StandardCharsets.UTF_8));
            outputStream.close();

            BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            String line;
            StringBuilder response = new StringBuilder();
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
            reader.close();

            String result = response.toString();
            JSONObject jsonObject = JSONObject.parseObject(result);
            Integer errorCode = jsonObject.getInteger("errorCode");
            if (errorCode == 0) {
                JSONArray translateResult = jsonObject.getJSONArray("translateResult").getJSONArray(0);
                String tgt = translateResult.getJSONObject(0).getString("tgt");
                String s = tgt.replaceAll("The ", "");
                return s.replaceAll(" ", "_").toLowerCase().replaceAll("_of_the", "").replaceAll("_of", "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }


}
