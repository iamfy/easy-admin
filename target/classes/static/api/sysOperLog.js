/**
 * 分页查询操作日志记录列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysOperLog/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询操作日志记录详细
 * @param id
 * @returns {*}
 */
function detail(operId) {
    return requests({
        url: '/admin/sysOperLog/query/' + operId,
        method: 'get'
    })
}

/**
 * 新增操作日志记录
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysOperLog/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改操作日志记录
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysOperLog/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除操作日志记录
 * @param id
 * @returns {*}
 */
function del(operId) {
    return requests({
        url: '/admin/sysOperLog/delete/' + operId,
        method: 'delete'
    })
}

/**
 * 导出操作日志记录
 *
 * @param query
 * @returns {*}
 */
function exportOperLog(query) {
    return requests({
        url: '/admin/sysOperLog/export',
        method: 'get',
        params: query
    })
}
